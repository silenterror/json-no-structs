package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {

	/*
				The reponse is json and looks something like this
		{
		  "people": [
		    {
		      "craft": "ISS",
		      "name": "Mark Vande Hei"
		    },
		    {
		      "craft": "ISS",
		      "name": "Matthias Maurer"
		    }
		  ],
		  "message": "success",
		  "number": 2g
		}

	*/

	// Call the endpoint for the data this shows current astronauts in space and their craft.
	response, err := http.Get("http://api.open-notify.org/astros.json")
	if err != nil {
		panic(err)
	}

	// defer the body close
	defer response.Body.Close()

	// read in the body
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}

	// create a data structure as close as we can to the response since
	// there are three top level strings with different key types we have
	// to make a map[string]interface.
	jsonMap := make(map[string]interface{})

	// Unmarshal the response body now in bytes into the jsonMap
	if err := json.Unmarshal(bodyBytes, &jsonMap); err != nil {
		panic(err)
	}

	// Show all the keys of the jsonMap
	for k := range jsonMap {
		fmt.Printf("Keys: %v\n", k)
	}

	// Marshal up the people map as it has an interface that we want to work with better
	mPeople, err := json.Marshal(jsonMap["people"])
	if err != nil {
		panic(err)
	}

	// Make a type that matches the data structure people
	mapArr := []map[string]string{}

	// Now unmarshal the marshalled jsonMap["people"]
	if err := json.Unmarshal(mPeople, &mapArr); err != nil {
		panic(err)
	}

	// iterate over the map showing we can now interact with name and craft
	// before when it was an interface we could not do that
	for _, m := range mapArr {
		fmt.Printf("Name: %v\nCraft: %v\n\n", m["name"], m["craft"])
	}

	// Print the message and number values. These were top level with people.
	fmt.Println(jsonMap["message"])
	fmt.Println(jsonMap["number"])

}
