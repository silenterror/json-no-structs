# json-no-structs

Using only go primitives I accept a json response from an url.
That url pulls data on astronauts currently in space.
This was chosen for simplicity as well as it contains 
different data structures. So json using primitives instead
of json tagged structs.
